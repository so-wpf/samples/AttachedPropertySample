﻿namespace AttachedPropertySample
{
    using System.Windows;
    using System.Windows.Media;

    public static class Icon
    {
        public static readonly DependencyProperty GeometryProperty = DependencyProperty.RegisterAttached(
            "Geometry",
            typeof (Geometry), 
            typeof (Icon),
            new PropertyMetadata(default(Geometry)));

        public static void SetGeometry(this FrameworkElement element, Geometry value)
        {
            element.SetValue(GeometryProperty, value);
        }

        public static Geometry GetGeometry(this FrameworkElement element)
        {
            return (Geometry) element.GetValue(GeometryProperty);
        }
    }
}
